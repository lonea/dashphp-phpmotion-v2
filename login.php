<?php
/**
 * Product: DashPHP Framework
 * Developers: CodeBox
 * Website: http://codebox.ca
 * Email: cs@codebox.ca
 * 
 * Unauthorized distribution of this script is prohibited
 * @copyright 2016 - Reliant-web
 */
require('./init.php');
require(BOOTS_DIR.'basicPages.php');
$smarty->caching = false;

global $obj;

if ($_GET["logout"]){	
	$obj->LogOut();
	$smarty->assign("logout", true);
}
elseif ($_SESSION["loggedin"]){
	$smarty->assign("loggedin", true);
}
elseif ($_POST["email"]){
	if ($obj->LoginByEmail($_POST["email"],$_POST["password"])){
		$smarty->assign("loggedin", $_SESSION["loggedin"]);
		$smarty->assign("username", $_SESSION["username"]);
	}else{
		$smarty->assign("loginfailed", true);
	}

}

loadTemplate("login"); 


?> 

