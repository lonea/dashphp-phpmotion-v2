<link rel="stylesheet" href="httsp://cdn.jsdelivr.net/pure/0.6.0/pure-min.css"/>

{if $loggedin == true}
	Congrats! You have logged in!<br/>
{elseif $loginfailed == true}
	Hey! Incorrect login information!<br/>
{elseif $logout == true}
	Hey! You have logged out successfully!<br/>
{/if}

{if $loggedin != true}
	<form class="pure-form" method="POST" action="login.php">
		<fieldset>
			<legend>Login</legend>

			<input type="email" placeholder="Email" name="email">
			<input type="password" placeholder="Password" name="password">

			<label for="remember">
				<input id="remember" type="checkbox"> Remember me
			</label>

			<button type="submit" class="pure-button pure-button-primary">Sign in</button>
		</fieldset>
	</form>
{/if}